package com.example.nearbylocations.util.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.nearbylocations.R

/**
 * This function loads images that api gives us in json and svg form.
 *
 * @param placeholder custom placeholder
 */
fun ImageView.loadImage(imageUrl: String, placeholder: Int) {
    Glide.with(context)
        .load(imageUrl)
        .placeholder(placeholder)
        .into(this)
}