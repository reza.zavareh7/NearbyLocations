package com.example.nearbylocations.util

import kotlinx.coroutines.flow.*

/**
 * A inline function for handle get data and cash it on data base
 * @param query a query from data base
 * @param fetch a api call
 * @param saveFetchResult save result of api call on data base
 * @param shouldFetch should get data from api call or not
 */
inline fun <ResultType, RequestType> networkBoundResource(
    crossinline query: () -> Flow<ResultType>,
    crossinline fetch: suspend () -> RequestType,
    crossinline saveFetchResult: suspend (RequestType) -> Unit,
    crossinline shouldFetch: (ResultType) -> Boolean = { true }
) = flow {
    val data = query().first()

    val flow = if (shouldFetch(data)) {
        emit(Resource.Loading(data))
        try {
            saveFetchResult(fetch())
            query().map { Resource.Success(it) }
        } catch (throwable: Throwable) {
            query().map {
                Resource.Error(data, throwable = throwable, message = throwable.message.toString())
            }
        }
    } else {
        query().map { Resource.Success(it) }
    }

    emitAll(flow)
}