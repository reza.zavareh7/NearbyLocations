package com.example.nearbylocations.util.extension

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.example.nearbylocations.R
import com.google.android.material.snackbar.Snackbar

/**
 * Show snackBar message.
 *
 * @param message Message to show as snackBar
 */
fun View.showSnackMessage(message: String, actionMessage: CharSequence? = null, action: ((View) -> Unit)? = null) {
    val snackBar = createSnackBar(message, actionMessage, action)
    val snackBarTitle: TextView = snackBar.view.findViewById(R.id.snackbar_text)
    snackBarTitle.layoutDirection = View.LAYOUT_DIRECTION_RTL
    snackBar.show()
}

/**
 * Show snackBar message for errors.
 *
 * @param message Message to show as snackBar
 */
fun View.showErrorSnackMessage(message: String) {
    val snackBar = createSnackBar(message)
    snackBar.setBackgroundTint(resources.getColor(R.color.red))
    val snackBarTitle: TextView = snackBar.view.findViewById(R.id.snackbar_text)
    snackBarTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
    snackBarTitle.layoutDirection = View.LAYOUT_DIRECTION_RTL
    snackBar.show()
}

/**
 * Create [Snackbar] instance
 *
 * @return [Snackbar] instance
 */
fun View.createSnackBar(message: String, actionMessage: CharSequence? = null, action: ((View) -> Unit)? = null): Snackbar {
    val snackBar =  Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    if (actionMessage != null && action != null) {
        snackBar.setAction(actionMessage) {
            action(this)
        }.show()
    } else {
        snackBar.show()
    }

    return snackBar
}


/**
 * Makes a view visible.
 *
 */
fun View.makeVisible() {
    visibility = View.VISIBLE
}

/**
 * Makes a view invisible.
 *
 */
fun View.makeInVisible() {
    visibility = View.INVISIBLE
}

/**
 * Makes a view gone.
 *
 */
fun View.makeGone() {
    visibility = View.GONE
}