package com.example.nearbylocations.data.repository

import androidx.room.withTransaction
import com.example.nearbylocations.data.local.dao.PlaceDAO
import com.example.nearbylocations.data.local.datastore.DataStorePreferences
import com.example.nearbylocations.data.local.db.PlaceDataBase
import com.example.nearbylocations.data.local.db.PlaceItem
import com.example.nearbylocations.data.network.retrofit.PlacesApi
import com.example.nearbylocations.util.Resource
import com.example.nearbylocations.util.networkBoundResource
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import javax.inject.Inject

/**
 * An interface for handle api calls about locations
 */
interface PlaceRepository {

    /**
     * Call nearby places api for get all nearby places of current location(latitude,longitude) of user
     * @param ll latitude and longitude of current location
     * @return a flow of place list [PlaceItem]
     */
    suspend fun nearbyPlaces(ll: String): Flow<Resource<List<PlaceItem>>>

    /**
     * Call place details api for get detail of a location
     * @param fsqId foursquare id
     * @return a flow of [PlaceItem]
     */
    suspend fun placeDetails(fsqId: String): Flow<Resource<PlaceItem?>>

    /**
     * Save location of user
     * @param location current location(latitude,longitude) of user
     */
    suspend fun saveUserLocation(location: String)

    /**
     * Get tha last location(latitude,longitude) of user
     *
     * @return a string of last location of user
     */
    suspend fun getUserLocation(): String
}

/**
 *  A class for implement [PlaceRepository] functions and return results
 *  @param api an interface [PlacesApi] for call api functions
 *  @param db data base of app [PlaceDataBase]
 *  @param dao a Dao of room data base [PlaceDAO] for insert and get data from data base[PlaceDataBase]
 *  @param dataStore a [DataStorePreferences] for keep some information on it like current location of user
 */
class PlaceRepositoryImpl @Inject constructor(
    private val api: PlacesApi,
    private val db: PlaceDataBase,
    private val dao: PlaceDAO,
    private val dataStore: DataStorePreferences
) : PlaceRepository {

    override suspend fun nearbyPlaces(ll: String) = networkBoundResource(
        query = {
            dao.getAllPlaces()
        },
        fetch = {
            delay(2000)
            api.nearbyLocations(latitudeLongitude = ll)
        },
        saveFetchResult = {
            if (!it.results.isNullOrEmpty()) {
                val places: List<PlaceItem> = it.results.map { resultItem ->
                    PlaceItem(
                        fsqId = resultItem.fsq_id,
                        name = resultItem.name,
                        address = resultItem.location.address,
                        icon = if (resultItem.categories.isNullOrEmpty()) null else
                            "${resultItem.categories.first().icon.prefix}bg_120${resultItem.categories.first().icon.suffix}",
                        distance = resultItem.distance.toString(),
                        placeDetail = dao.getPlaceInfo(resultItem.fsq_id).first()?.placeDetail
                    )
                }
                db.withTransaction {
                    dao.deleteAllPlaces()
                    dao.insertPlaces(places)
                }
            }
        }
    )


    override suspend fun placeDetails(fsqId: String) = networkBoundResource(
        query = {
            dao.getPlaceInfo(fsqId)
        },
        fetch = {
            delay(2000)
            api.placeDetail(foursquareId = fsqId)
        },
        saveFetchResult = {
            dao.savePlaceInfo(fsqId, it)
        }
    )

    override suspend fun saveUserLocation(location: String) {
        dataStore.storeUserLocation(location)
    }

    override suspend fun getUserLocation(): String {
        return dataStore.userLocation.first()
    }
}