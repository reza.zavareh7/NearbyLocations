package com.example.nearbylocations.feature.nearbyplaces

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.location.LocationManager.NETWORK_PROVIDER
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.nearbylocations.R
import com.example.nearbylocations.base.BaseFragment
import com.example.nearbylocations.databinding.FragmentNearbyPlacesBinding
import com.example.nearbylocations.feature.nearbyplaces.adapter.NearbyPlaceAdapter
import com.example.nearbylocations.util.Resource
import com.example.nearbylocations.util.extension.*
import dagger.hilt.android.AndroidEntryPoint

/**
 * A [Fragment] for show nearby places page that extended from [BaseFragment]
 */
@AndroidEntryPoint
class NearbyPlacesFragment :
    BaseFragment<FragmentNearbyPlacesBinding>(R.layout.fragment_nearby_places) {
    private val viewModel: NearbyPlacesViewModel by viewModels()
    private lateinit var adapter: NearbyPlaceAdapter
    private var mCurrentLocationCoordinate = ""
    private var currentLocation: Location? = null
    private var locationManager: LocationManager? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        listeners()
        observers()
    }

    private fun init() {
        locationManager =
            requireContext().getSystemService(LOCATION_SERVICE) as LocationManager?
        setUpRecyclerView()
        viewModel.getUserLocation()
    }

    private fun listeners() {
        val listener = LocationListener { location ->
            if (currentLocation != null && location.distanceTo(currentLocation) >= 100) {
                handleGetNearbyPlaces(location)
            }
        }

        try {
            locationManager!!.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                10000,
                100.0f,
                listener
            )
        } catch (ex: SecurityException) {
            Log.d("SecurityException", ex.message.toString())
        }
    }

    private fun observers() {
        collectLifecycleFlow(viewModel.nearbyPlaces) { state ->
            when (state) {
                is Resource.Success -> {
                    state.data?.let {
                        binding.progressBar.makeGone()
                        binding.emptyData.makeGone()
                        adapter.submitList(it)
                        viewModel.saveUserLocation(mCurrentLocationCoordinate)
                    }
                }
                is Resource.Loading -> {
                    binding.progressBar.makeVisible()
                }
                is Resource.Error -> {
                    binding.progressBar.makeGone()
                    if (!state.data.isNullOrEmpty()) {
                        binding.emptyData.makeGone()
                        adapter.submitList(state.data)
                    } else {
                        binding.emptyData.makeVisible()
                        requireView().showErrorSnackMessage(getString(R.string.network_error))
                    }
                }
            }
        }

        collectLifecycleFlow(viewModel.userLocation) {
            it?.let { userLocation ->
                if (userLocation.isNotEmpty()) {
                    mCurrentLocationCoordinate = userLocation
                    viewModel.nearbyPlaces(userLocation)
                }
                requestPermission()
            }
        }
    }

    private fun requestPermission() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED -> {
                requireView().showSnackMessage(getString(R.string.permission_granted))
                getCurrentLocation()
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                ACCESS_COARSE_LOCATION
            ) -> {
                requireView().showSnackMessage(
                    getString(R.string.permission_required),
                    getString(R.string.ok)
                ) {
                    requestPermission.launch(ACCESS_COARSE_LOCATION)
                }
            }
            else -> {
                requestPermission.launch(ACCESS_COARSE_LOCATION)
            }
        }
    }

    private fun getCurrentLocation() {
        if (mCurrentLocationCoordinate.isEmpty())
            locationManager?.let { locationManager ->
                getLastKnownLocationObject(locationManager)?.let { location ->
                    handleGetNearbyPlaces(location)
                }
            }
    }

    private fun getLastKnownLocationObject(locationManager: LocationManager): Location? {
        try {
            if (locationManager.isProviderEnabled(NETWORK_PROVIDER)) {
                // Location is always null on S7 (only)
                return locationManager.getLastKnownLocation(NETWORK_PROVIDER)
            }
        } catch (ex: SecurityException) {
            Log.d("SecurityException", ex.message.toString())
        }
        return null
    }

    private fun handleGetNearbyPlaces(location: Location) {
        currentLocation = location
        mCurrentLocationCoordinate = "${currentLocation!!.latitude},${currentLocation!!.longitude}"
        viewModel.nearbyPlaces(mCurrentLocationCoordinate)
    }

    private fun setUpRecyclerView() {
        adapter = NearbyPlaceAdapter {
            findNavController().navigate(
                NearbyPlacesFragmentDirections.actionNearbyPlacesFragmentToPlaceDetailFragment(
                    fsqId = it
                )
            )
        }
        binding.nearbyPlacesRecycler.adapter = adapter
    }

    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission())
        { isGranted: Boolean ->
            if (isGranted) {
                getCurrentLocation()
            } else
                Toast.makeText(requireContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show()
        }
}