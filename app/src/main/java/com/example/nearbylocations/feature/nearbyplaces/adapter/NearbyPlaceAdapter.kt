package com.example.nearbylocations.feature.nearbyplaces.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.nearbylocations.R
import com.example.nearbylocations.data.local.db.PlaceItem
import com.example.nearbylocations.databinding.ItemNearbyPlacesListBinding
import com.example.nearbylocations.util.extension.loadImage

/**
 * A [ListAdapter] that handle show nearby place list data to [RecyclerView]
 * @param getItem an lambda function for call back foursquare id of item after click on it
 */
class NearbyPlaceAdapter(private val getItem: (String) -> Unit) :
    ListAdapter<PlaceItem, NearbyPlaceAdapter.NearbyPlaceViewHolder>(
        NearbyPlaceDiffUtil()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NearbyPlaceViewHolder {
        return NearbyPlaceViewHolder(
            ItemNearbyPlacesListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false,
            ), getItem
        )
    }

    override fun onBindViewHolder(holder: NearbyPlaceViewHolder, position: Int) {
        holder.bindData(getItem(position))
    }

    /**
     * A [RecyclerView.ViewHolder] to handle show nearby place data on view
     * @param binding a [ItemNearbyPlacesListBinding] for handle item views
     * @param getItem an lambda function for call back foursquare id of item after click on it
     */
    class NearbyPlaceViewHolder(
        private val binding: ItemNearbyPlacesListBinding,
        private val getItem: (String) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        /**
         * Bind data on every item view
         * @param data a [PlaceItem] that contain a nearby place information
         */
        fun bindData(data: PlaceItem) {
            val distance = data.distance
            data.name?.let { binding.title.text = it }
            data.address?.let { address ->
                if (!distance.isNullOrEmpty())
                    binding.address.text = binding.address.context.getString(
                        R.string.address_complete, distance, address
                    )
                else
                    binding.address.text = address
            }
            data.icon?.let {
                binding.icon.loadImage(it, R.drawable.ic_location_city)
            }
            itemView.setOnClickListener { getItem(data.fsqId) }
        }
    }
}

/**
 * A [DiffUtil] to prevent create again same items on [NearbyPlaceAdapter]
 */
class NearbyPlaceDiffUtil : DiffUtil.ItemCallback<PlaceItem>() {
    override fun areItemsTheSame(oldItem: PlaceItem, newItem: PlaceItem) = oldItem == newItem

    override fun areContentsTheSame(oldItem: PlaceItem, newItem: PlaceItem) =
        oldItem.fsqId == newItem.fsqId
}