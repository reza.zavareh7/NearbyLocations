package com.example.nearbylocations.feature.nearbyplaces

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nearbylocations.data.repository.PlaceRepository
import com.example.nearbylocations.data.local.db.PlaceItem
import com.example.nearbylocations.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A [ViewModel] for handle some functions on [NearbyPlacesFragment]
 * @param repository a [PlaceRepository] for handle api calls
 */
@HiltViewModel
class NearbyPlacesViewModel @Inject constructor(private val repository: PlaceRepository) :
    ViewModel() {

    private val _nearbyPlaces =
        MutableStateFlow<Resource<List<PlaceItem>>>(Resource.Loading())
    val nearbyPlaces = _nearbyPlaces.asStateFlow()

    private val _userLocation = MutableStateFlow<String?>(null)
    val userLocation = _userLocation.asStateFlow()

    /**
     * Get nearby places
     * @param ll latitude and longitude of current location
     */
    fun nearbyPlaces(ll: String) {
        viewModelScope.launch {
            repository.nearbyPlaces(ll)
                .flowOn(Dispatchers.IO)
                .collect { _nearbyPlaces.value = it }
        }
    }

    /**
     * Get tha last location(latitude,longitude) of user
     */
    fun getUserLocation() {
        viewModelScope.launch(Dispatchers.IO) {
            _userLocation.value = repository.getUserLocation()
        }
    }

    /**
     * Save location of user
     * @param currentLocation current location(latitude,longitude) of user
     */
    fun saveUserLocation(currentLocation: String) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.saveUserLocation(currentLocation)
        }
    }
}