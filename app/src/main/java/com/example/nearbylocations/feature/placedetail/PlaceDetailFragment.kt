package com.example.nearbylocations.feature.placedetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.nearbylocations.R
import com.example.nearbylocations.base.BaseFragment
import com.example.nearbylocations.data.local.db.PlaceItem
import com.example.nearbylocations.databinding.FragmentPlaceDetailBinding
import com.example.nearbylocations.util.Resource
import com.example.nearbylocations.util.extension.*
import dagger.hilt.android.AndroidEntryPoint

/**
 * A [Fragment] for show Place detail page that extended from [BaseFragment]
 */
@AndroidEntryPoint
class PlaceDetailFragment :
    BaseFragment<FragmentPlaceDetailBinding>(R.layout.fragment_place_detail) {

    private val viewModel: PlaceDetailViewModel by viewModels()
    private val args: PlaceDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        listeners()
        observers()
    }

    private fun init() {
        viewModel.getPlaceDetail(args.fsqId)
    }

    private fun listeners() {
        binding.back.setOnClickListener { findNavController().popBackStack() }
    }

    private fun observers() {
        collectLifecycleFlow(viewModel.placeDetail) { state ->
            binding.emptyData.makeGone()
            when (state) {
                is Resource.Success -> {
                    state.data?.let {
                        binding.progressBar.makeGone()
                        binding.group.makeVisible()
                        bindDataToView(it)
                    }
                }
                is Resource.Loading -> {
                    binding.progressBar.makeVisible()
                }
                is Resource.Error -> {
                    binding.progressBar.makeGone()
                    if (state.data?.placeDetail != null) {
                        binding.group.makeVisible()
                        bindDataToView(state.data)
                    } else {
                        binding.emptyData.makeVisible()
                        requireView().showErrorSnackMessage(getString(R.string.network_error))
                    }
                }
            }
        }
    }

    private fun bindDataToView(placeItem: PlaceItem) {
        placeItem.placeDetail?.let { placeDetail ->
            binding.title.text = placeDetail.name
            placeItem.icon?.let { binding.icon.loadImage(it, R.drawable.ic_location_city) }
            placeDetail.location?.let {
                binding.address.text = getString(R.string.address, it.address)
                binding.addressExtended.text = getString(R.string.address_extended, it.address_extended)
                binding.country.text = getString(R.string.country, it.country)
                binding.locality.text = getString(R.string.locality, it.locality)
                binding.postalCode.text = getString(R.string.postal_code, it.postcode)
                binding.region.text = getString(R.string.region, it.region)
            }
        }
    }
}