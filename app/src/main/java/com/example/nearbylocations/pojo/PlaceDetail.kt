package com.example.nearbylocations.pojo

data class PlaceDetail(
    val categories: List<Category>? = null,
    val chains: List<Any>? = null,
    val fsq_id: String,
    val geocodes: Geocodes? = null,
    val location: Location? = null,
    val name: String,
    val related_places: RelatedPlaces? = null,
    val timezone: String? = null
)