package com.example.nearbylocations.di

import android.content.Context
import com.example.nearbylocations.data.local.datastore.DataStorePreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * A singleton module for provide [DataStorePreferences] to write and read information on it
 */
@Module
@InstallIn(SingletonComponent::class)
object DataStoreModule {

    /**
     * Provide [DataStorePreferences]
     * @param appContext application context
     * @return [DataStorePreferences]
     */
    @Provides
    @Singleton
    fun provideDataStore(@ApplicationContext appContext: Context): DataStorePreferences =
        DataStorePreferences(appContext)
}
