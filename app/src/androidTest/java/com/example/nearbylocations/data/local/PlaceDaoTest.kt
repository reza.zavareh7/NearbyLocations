package com.example.nearbylocations.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.nearbylocations.data.local.dao.PlaceDAO
import com.example.nearbylocations.data.local.db.PlaceDataBase
import com.example.nearbylocations.data.local.db.PlaceItem
import com.example.nearbylocations.pojo.PlaceDetail
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class PlaceDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: PlaceDataBase
    private lateinit var dao: PlaceDAO
    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(), PlaceDataBase::class.java
        ).allowMainThreadQueries().build()
        dao = database.placeDAO()
    }

    @After
    fun teardown() {
        database.close()
    }


    @Test
    fun insertPlaceItems() = runBlocking {
        val placeItem = PlaceItem("12345", name = "iran")
        val placeItems =  listOf(placeItem)
        dao.insertPlaces(placeItems)
        val items = dao.getAllPlaces().first()
        assertThat(items.first().name).isEqualTo(placeItem.name)
    }

    @Test
    fun deletePlaceItems() = runBlocking {
        val placeItem = PlaceItem("123456", name = "iran")
        val placeItems =  listOf(placeItem)
        dao.insertPlaces(placeItems)
        dao.deleteAllPlaces()
        val items = dao.getAllPlaces().first()
        assertThat(items).isEmpty()
    }

    @Test
    fun getPlaceInfo() = runBlocking {
        val placeItem = PlaceItem("123456", name = "iran")
        val placeItems =  listOf(placeItem)
        dao.insertPlaces(placeItems)
        val placeDetail = PlaceDetail(fsq_id = placeItem.fsqId, name = placeItem.name!!)
        dao.savePlaceInfo(placeItem.fsqId, placeDetail)
        val placeInfo = dao.getPlaceInfo(placeItem.fsqId).first()
        assertThat(placeInfo.placeDetail!!.fsq_id).isEqualTo(placeItem.fsqId)
    }
}