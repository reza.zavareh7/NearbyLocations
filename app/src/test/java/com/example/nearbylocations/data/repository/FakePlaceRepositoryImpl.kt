package com.example.nearbylocations.data.repository

import com.example.nearbylocations.data.local.db.PlaceItem
import com.example.nearbylocations.pojo.PlaceDetail
import com.example.nearbylocations.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakePlaceRepositoryImpl : PlaceRepository {
    private var shouldReturnNetworkError = false
    private var userLocation = ""

    fun setShouldReturnNetworkError(value: Boolean) {
        shouldReturnNetworkError = value
    }

    override suspend fun nearbyPlaces(ll: String): Flow<Resource<List<PlaceItem>>> {
        return flow {
            if (shouldReturnNetworkError) {
                emit(Resource.Error(data = null, throwable = null, message = "Have Error"))
            } else {
                val data = listOf(PlaceItem(fsqId = "12345", name = "iran"))
                emit(Resource.Success(data))
            }
        }
    }

    override suspend fun placeDetails(fsqId: String): Flow<Resource<PlaceItem>> {
        return flow {
            if (shouldReturnNetworkError) {
                emit(Resource.Error(data = null, throwable = null, message = "Have Error"))
            } else {
                val data = PlaceItem(
                    fsqId = fsqId,
                    name = "iran",
                    placeDetail = PlaceDetail(fsq_id = fsqId, name = "iran")
                )
                emit(Resource.Success(data))
            }
        }
    }

    override suspend fun saveUserLocation(location: String) {
        userLocation = location
    }

    override suspend fun getUserLocation() = userLocation

}