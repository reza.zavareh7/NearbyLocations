package com.example.nearbylocations.feature.placedetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.example.nearbylocations.MainCoroutineRule
import com.example.nearbylocations.data.repository.FakePlaceRepositoryImpl
import com.example.nearbylocations.feature.nearbyplaces.NearbyPlacesViewModel
import com.example.nearbylocations.util.Resource
import com.google.common.truth.Truth
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class PlaceDetailViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: PlaceDetailViewModel
    private lateinit var repository: FakePlaceRepositoryImpl
    private lateinit var foursquareId: String

    @Before
    fun setUp() {
        repository = FakePlaceRepositoryImpl()
        viewModel = PlaceDetailViewModel(repository)
        foursquareId = "1234567"
    }


    @Test
    fun `when getPlaceDetail calls, Then returns success state by data`() = runBlocking {
        viewModel.placeDetail.test {
            viewModel.getPlaceDetail(foursquareId)
            awaitItem()
            val emission = awaitItem()
            if (emission is Resource.Success)
                Truth.assertThat(emission.data!!.fsqId).isEqualTo(foursquareId)
        }
    }

    @Test
    fun `when getPlaceDetail calls, Then returns error state`() =
        runBlocking {
            repository.setShouldReturnNetworkError(true)
            viewModel.placeDetail.test {
                viewModel.getPlaceDetail(foursquareId)
                awaitItem()
                val emission = awaitItem()
                if (emission is Resource.Error)
                    Truth.assertThat(emission.message!!).isEqualTo("Have Error")
            }
        }
}