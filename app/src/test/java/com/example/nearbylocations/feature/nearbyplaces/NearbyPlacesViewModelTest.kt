package com.example.nearbylocations.feature.nearbyplaces

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import app.cash.turbine.test
import com.example.nearbylocations.MainCoroutineRule
import com.example.nearbylocations.data.repository.FakePlaceRepositoryImpl
import com.example.nearbylocations.util.Resource
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class NearbyPlacesViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: NearbyPlacesViewModel
    private lateinit var repository: FakePlaceRepositoryImpl
    private lateinit var userLocation: String

    @Before
    fun setUp() {
        repository = FakePlaceRepositoryImpl()
        viewModel = NearbyPlacesViewModel(repository)
        userLocation = "41.021,35.548"
    }


    @Test
    fun `when getNearbyPlaces calls, Then  returns success state by data`() = runBlocking {
        viewModel.nearbyPlaces.test {
            viewModel.nearbyPlaces(userLocation)
            awaitItem()
            val emission = awaitItem()
            if (emission is Resource.Success)
                assertThat(emission.data!!.first().name).isEqualTo("iran")
        }
    }

    @Test
    fun `When getNearbyPlaces calls, Then returns error state`() =
        runBlocking {
            repository.setShouldReturnNetworkError(true)
            viewModel.nearbyPlaces.test {
                viewModel.nearbyPlaces(userLocation)
                awaitItem()
                val emission = awaitItem()
                if (emission is Resource.Error)
                    assertThat(emission.message!!).isEqualTo("Have Error")
            }
        }

    @Test
    fun `When getUserLocation, Then get user location after save that`() = runBlocking {
        viewModel.userLocation.test {
            viewModel.saveUserLocation(userLocation)
            viewModel.getUserLocation()
            awaitItem()
            val emission = awaitItem()
            if (emission != null)
                assertThat(emission).isEqualTo(userLocation)
        }
    }
}